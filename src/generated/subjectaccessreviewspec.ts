/* tslint:disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

/**
 * SubjectAccessReviewSpec is a description of the access request.  Exactly one of ResourceAuthorizationAttributes and NonResourceAuthorizationAttributes must be set
 */
export interface Subjectaccessreviewspec {
  /**
   * Extra corresponds to the user.Info.GetExtra() method from the authenticator.  Since that is input to the authorizer it needs a reflection here.
   */
  extra?: {
    [k: string]: (string | null)[];
  };
  /**
   * Groups is the groups you're testing for.
   */
  group?: (string | null)[];
  /**
   * NonResourceAttributes includes the authorization attributes available for non-resource requests to the Authorizer interface
   */
  nonResourceAttributes?: {
    /**
     * Path is the URL path of the request
     */
    path?: string | null;
    /**
     * Verb is the standard HTTP verb
     */
    verb?: string | null;
  };
  /**
   * ResourceAttributes includes the authorization attributes available for resource requests to the Authorizer interface
   */
  resourceAttributes?: {
    /**
     * Group is the API Group of the Resource.  "*" means all.
     */
    group?: string | null;
    /**
     * Name is the name of the resource being requested for a "get" or deleted for a "delete". "" (empty) means all.
     */
    name?: string | null;
    /**
     * Namespace is the namespace of the action being requested.  Currently, there is no distinction between no namespace and all namespaces "" (empty) is defaulted for LocalSubjectAccessReviews "" (empty) is empty for cluster-scoped resources "" (empty) means "all" for namespace scoped resources from a SubjectAccessReview or SelfSubjectAccessReview
     */
    namespace?: string | null;
    /**
     * Resource is one of the existing resource types.  "*" means all.
     */
    resource?: string | null;
    /**
     * Subresource is one of the existing resource types.  "" means none.
     */
    subresource?: string | null;
    /**
     * Verb is a kubernetes resource API verb, like: get, list, watch, create, update, delete, proxy.  "*" means all.
     */
    verb?: string | null;
    /**
     * Version is the API Version of the Resource.  "*" means all.
     */
    version?: string | null;
  };
  /**
   * UID information about the requesting user.
   */
  uid?: string | null;
  /**
   * User is the user you're testing for. If you specify "User" but not "Group", then is it interpreted as "What if User were not a member of any groups
   */
  user?: string | null;
}
