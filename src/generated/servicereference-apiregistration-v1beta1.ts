/* tslint:disable */
/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */

/**
 * ServiceReference holds a reference to Service.legacy.k8s.io
 */
export interface ServicereferenceApiregistrationV1Beta1 {
  /**
   * Name is the name of the service
   */
  name?: string | null;
  /**
   * Namespace is the namespace of the service
   */
  namespace?: string | null;
}
