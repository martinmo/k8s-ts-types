import * as fs from "fs";

const srcPath = "src/generated/";
const targetFile = "./src/index.ts";
const files = fs.readdirSync(srcPath);

const exportLines = files.map(fileName => {
    const packageName = fileName.replace(".ts", "");
    return "export * from './generated/" + packageName + "';"
}).join("\n");

fs.writeFileSync(targetFile, exportLines);

console.log("generated " + targetFile);