import {compileFromFile} from 'json-schema-to-typescript'
import * as fs from "fs";


const srcPath = "./kubernetes-json-schema/v1.14.0-standalone-strict/";
const targetPath = "./src/generated/";
const files = fs.readdirSync(srcPath);
const excludes = ["all.json", "_definitions.json"];

async function run() {
    for (let file of files) {

        if (excludes.indexOf(file) >= 0) {
            console.log("skipping " + file);
            continue
        }

        file = file.replace(".json", "");

        const srcFile = srcPath + file + '.json';
        const targetFile = targetPath + file + '.ts';

        if (fs.existsSync(targetFile)) {
            console.log("skipping as " + targetFile + " exists");
            continue
        }

        console.log(`generating types from ${srcFile} to ${targetFile}`);

        const types = await compileFromFile(srcFile, {cwd: srcPath});
        fs.writeFileSync(targetFile, types);
    }
}

run()
    .catch(console.error)
    .then(() => console.log("done"));
