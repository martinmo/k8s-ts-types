/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */
/**
 * LoadBalancerStatus represents the status of a load-balancer.
 */
export interface Loadbalancerstatus {
    /**
     * Ingress is a list containing ingress points for the load-balancer. Traffic intended for the service should be sent to these ingress points.
     */
    ingress?: {
        /**
         * Hostname is set for load-balancer ingress points that are DNS based (typically AWS load-balancers)
         */
        hostname?: string | null;
        /**
         * IP is set for load-balancer ingress points that are IP based (typically GCE or OpenStack load-balancers)
         */
        ip?: string | null;
    }[];
}
