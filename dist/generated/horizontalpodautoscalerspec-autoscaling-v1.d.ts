/**
 * This file was automatically generated by json-schema-to-typescript.
 * DO NOT MODIFY IT BY HAND. Instead, modify the source JSONSchema file,
 * and run json-schema-to-typescript to regenerate this file.
 */
/**
 * specification of a horizontal pod autoscaler.
 */
export interface HorizontalpodautoscalerspecAutoscalingV1 {
    /**
     * upper limit for the number of pods that can be set by the autoscaler; cannot be smaller than MinReplicas.
     */
    maxReplicas: number;
    /**
     * lower limit for the number of pods that can be set by the autoscaler, default 1.
     */
    minReplicas?: number;
    /**
     * CrossVersionObjectReference contains enough information to let you identify the referred resource.
     */
    scaleTargetRef: {
        /**
         * API version of the referent
         */
        apiVersion?: string | null;
        /**
         * Kind of the referent; More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds"
         */
        kind: string;
        /**
         * Name of the referent; More info: http://kubernetes.io/docs/user-guide/identifiers#names
         */
        name: string;
    };
    /**
     * target average CPU utilization (represented as a percentage of requested CPU) over all the pods; if not specified the default autoscaling policy will be used.
     */
    targetCPUUtilizationPercentage?: number;
}
