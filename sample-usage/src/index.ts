import {DeploymentAppsV1} from "@martinmoeller/k8s-ts-types"

const deployment: DeploymentAppsV1 = {

    apiVersion: "apps/v1",

    kind: "Deployment",

    metadata: {
        name: "nginx-deployment",
        labels: {
            "name": "nginx-deployment"
        }
    },

    spec: {
        selector: {
            matchLabels: {
                name: "nginx"
            }
        },

        template: {
            metadata: {
                labels: {
                    name: "nginx"
                }
            },

            spec: {
                containers: [{
                    name: "nginx",
                    image: "nginx"
                }]
            }
        }
    }
};

console.log(JSON.stringify(deployment));
